// move_semantics2.rs
// Make me compile without changing line 13 or moving line 10!
// Execute `rustlings hint move_semantics2` for hints :)


fn main() {
    let vec0 = Vec::new();

    // method 1
    // let vectemp = vec0.to_vec();
    // let mut vec1 = fill_vec(vectemp);

    // method 2
    // let mut vec1 = fill_vec_borrowed(&vec0);

    // method 3
    let mut vectemp = vec0.to_vec();
    fill_vec_mutator(&mut vectemp);
    let mut vec1 = vectemp.to_vec();


    // Do not change the following line!
    println!("{} has length {} content `{:?}`", "vec0", vec0.len(), vec0);

    vec1.push(88);

    println!("{} has length {} content `{:?}`", "vec1", vec1.len(), vec1);
}

fn fill_vec(vec: Vec<i32>) -> Vec<i32> {
    let mut vec = vec;

    vec.push(22);
    vec.push(44);
    vec.push(66);

    vec
}


fn fill_vec_borrowed(vec: &Vec<i32>) -> Vec<i32> {
    let mut vec = vec.to_vec();

    vec.push(22);
    vec.push(44);
    vec.push(66);

    vec
}


fn fill_vec_mutator(vec: &mut Vec<i32>) {
    vec.push(22);
    vec.push(44);
    vec.push(66);
}


